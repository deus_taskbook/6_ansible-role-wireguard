# 1. Написать плейбук поднимающий VPN-сервер
Done.
# 2. Вынести tasks в набор ролей с конфигурациями через переменные
Done.

wireguard_server_ip: ip сервера (по дефолту подставляется из переменных ansible)
wireguard_server_int: интерфейс сервера (по дефолту подставляется из переменных ansible)
wireguard_server_port: порт wireguard
wireguard_network: внутренняя сеть vpn
wireguard_client_install: установка и настройка клиента
wireguard_client_ip: ip клиента
wireguard_client_name: username клиента=имя конфига для peer
wireguard_public_dns: DNS-сервер для клиента
wireguard_keepalive: интервал keepalive пакетов
wireguard_allowed_ip: с каких ip разрешать подключения
wireguard_portal_install: установка gui https://github.com/h44z/wg-portal
# 3. При завершении работы playbook отдает артефакты - логин/пароль/конфиг/ключи
Done.
# 4. Автотест ролей, версионирование и отдельные репо для каждой роли. Playbook подключает роли из galaxy
+\- версионирование есть, установка из galaxy есть. Остального нет.
# 5. Playbook в репозитории, настроен CI - при добавлении нового пользователя автоматически добавляется пользователь в конфиг сервера
-
# 6. Поднять GUI для VPN
Done.